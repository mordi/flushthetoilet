# README #

### What is this repository for? ###

this repository is made to build the "FlushTheToilet" challenge in every linux machine.
### How do I get set up? ###

* first clone this repository to your local linux machine.

```
git clone https://gitlab.com/mordi/flushthetoilet.git
```
* add the wanted users to the file [usersmgmt.txt](src/usersmgmt.txt) (based on the [example file](rsc/usersmgmt.txt.example))
* open terminal and run:

```sh 
sudo su
make
make install
```
or, if you are on centos:
```sh 
sudo su
make centos
make install
```
### Solution ###

* the flag is "LRRHFunny"