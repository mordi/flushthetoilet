compile: 
    gcc src/LetMeX.c -o LetMeX

deps-ubuntu:
    apt-get update
    apt-get install gcc
    apt-get install toilet

deps-centos:
    yum check-update
    yum install gcc
    yum install toilet

install: 
    chmod 444 LetMeX
    cp LetMeX /etc/perl/Net/
    ./src/usersmgmt add

clean: 
    rm LetMeX /etc/perl/Net/LetMeX 
    ./src/usersmgmt del
